/* replace the <ppid> tag with an actual PowerPack id to identify formulas with syntax errors
select * from (select 
    pp.ppid as powerpack_id
  , pp.name as powerpack_name
  , da.aid as dynamicapp_id
  , da.name as dynamicapp_name
  , daa.alert_id
  , daa.formula as alert_formula
  , case daa.formula regexp ' ?result'
      when 1 then 
        case daa.formula regexp 'result\\(.+?\\)'
          when 1 then 'present and ok'
          else 'present and not ok'
        end
	  when 0 then 'not present'
    end as resulttest
	
  , case daa.formula regexp 'threshold\\('
      when 1 then 
        case daa.formula regexp 'threshold\\(''?t_\\d+''?\\)'
          when 1 then 'present and ok'
          else 'present and not ok'
        end
	  when 0 then 'not present'
    end as thresholdtest

  , case daa.formula regexp 'alert\\('
      when 1 then 
        case daa.formula regexp 'alert\\(''?t_\\d+''?\\)'
          when 1 then 'present and ok'
          else 'present and not ok'
        end
	  when 0 then 'not present'
    end as alerttest

  , case daa.formula regexp 'label='
      when 1 then 
        case daa.formula regexp 'label=''?.+?'''
          when 1 then 'present and ok'
          else 'present and not ok'
        end
	  when 0 then 'not present'
    end as labeltest
  , daa.message as alert_message
  from master.powerpack as pp
   inner join master.dynamic_app as da on da.ppguid = pp.ppguid
   left outer join master.dynamic_app_alerts as daa on da.app_guid = daa.app_guid
  ) as innerq
  where powerpack_id = <ppid>
  and (
	resulttest = 'present and not ok' or 
	thresholdtest = 'present and not ok' or
	alerttest = 'present and not ok' or
	labeltest = 'present and not ok'
  )