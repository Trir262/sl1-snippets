select cred_id
, ps_account_type # 0 = Local, 1 = Active Directory
, ps_encrypted # 0 = not secure, 1 = secure
, ps_ad_domain, ps_ad_host, ps_proxy_host
from master.system_credentials_powershell
order by ps_ad_domain