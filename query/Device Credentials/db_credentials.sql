select sc.cred_id, sc.cred_name, sc.cred_user
    #, sc.cred_pwd # is commented out because it''s value is hashed
    , sc.cred_host, sc.cred_port
    , dct.name as credential_type_name
    , sc.cred_timeout
    , if(sc.all_orgs=1,'all organizations', maco.orgs )
    , mdctdb.name as db_type
    , mscdb.db_name
    , mscdb.db_sid
    , mscdb.db_connect
from master.system_credentials as sc
inner join master.definitions_credential_types dct on sc.cred_type=dct.cred_type
inner join master.system_credentials_db as mscdb on mscdb.cred_id = sc.cred_id
inner join master.definitions_credential_types_db as mdctdb on mdctdb.db_type = mscdb.db_type
left join (
    select co.cred_id, group_concat(o.company separator ',') as orgs 
    from master_access.credentials_organizations as co 
    inner join master_biz.organizations as o on co.roa_id = o.roa_id
    group by cred_id
) as maco on sc.cred_id = maco.cred_id