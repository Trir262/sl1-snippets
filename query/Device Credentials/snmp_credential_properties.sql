select cred_id
, snmp_version # 1 = v1, 2 = v2c, 3 = 3
#, snmp_ro_community # hidden because it is hashed
#, snmp_rw_community # hidden because it is hashed
, snmp_timeout # time in milliseconds
, snmp_retries
, snmpv3_auth_proto
, snmpv3_sec_level
, snmpv3_priv_proto
# , snmpv3_priv_pwd # hidden because it is hashed
, snmpv3_context
, snmpv3_engine_id
from master.system_credentials_snmp;