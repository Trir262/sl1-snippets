select scc.cred_id
, scc.curl_url
, scc.curl_timeout
, scc.curl_proxy_ip
, scc.curl_proxy_port
, scc.curl_proxy_acct
, scc.curl_proxy_passwd
, scc.curl_ssl_mode
, scc.curl_encoding
, scc.curl_post_or_get
, scc.curl_http_version
#, scc.curl_request_pwd # hidden because it is hashed
, scc.curl_request_sub_1
, scc.curl_request_sub_2
, scc.curl_request_sub_3
, scc.curl_request_sub_4
, scc.curl_header
, scc.curl_options
, scch.header
, scco.opt
, scco.value
from master.system_credentials_curl as scc
left join master.system_credentials_curl_headers as scch on scc.cred_id = scch.cred_id
left join master.system_credentials_curl_opts as scco on scc.cred_id = scco.cred_id;