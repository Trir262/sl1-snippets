# Get Events Linked to PowerPacks

This query shows all information about events that are linked to a specific powerpack.
replace the text `<Enter your powerpack name here>` with the name of your powerpack to get the contents.
The query can also be reversed by modifying the `where` clause to:
~~~sql
where policyname ='<your event policy name>'
~~~