select * from (
	select pp.ppid as ppid, pp.ppguid as ppguid, pp.name as ppname
	, da.aid as dynapid, da.name as daname, da.ppguid as dynapppguid
	, des.descr as esource, pe.id as policyid, pe.ename as policyname, pe.ppguid as policyppguid
	from master.powerpack as pp 
	inner join master.dynamic_app as da on pp.ppguid = da.ppguid
	inner join master.policies_events as pe on da.app_guid = pe.app_guid
	inner join master.definitions_event_sources as des on pe.esource = des.esource
	union
	select pp.ppid, pp.ppguid, pp.name
	, '', '', ''
	, des.descr as esource, pe.id, pe.ename, pe.ppguid
	from master.powerpack as pp
	inner join master.policies_events as pe on pp.ppguid = pe.ppguid
	inner join master.definitions_event_sources as des on pe.esource = des.esource
) as innerq
where ppname = '<enter your powerpack name here>'
