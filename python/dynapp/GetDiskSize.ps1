$Disks = Get-PSDrive -name [CD] # Get all disk with a 'C' and a 'D' as drive letter
if ($Disks) { #make sure we have any data to start with
    $Results = @() # this value will store all results we want to send back to ScienceLogic
    foreach ($Disk in $Disks) {
        $Results += New-Object -TypeName psobject -property @{
            keyfield = $Disk.Name
            capacity = $Disk.Used + $Disk.free
            freespace = $Disk.free
        }
    }
}
Write-Output ($Results | Select-Object keyfield, * -ErrorAction SilentlyContinue | format-list | Out-String )