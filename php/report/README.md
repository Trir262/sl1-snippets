# PHP Reports

All shareable report code goes here. Please respect the following convention

* A folder with the name of the report, within each folder:
  * a readme.md file with information about the report
  * an input.md file with information about the input form
  * a report.php file with the actual report code

A zipfile 'reporttemplate.zip' is available with this structure.

* Just extract it
* Rename the directory to your report name
* Enjoy!